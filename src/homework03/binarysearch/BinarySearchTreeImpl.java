package homework03.binarysearch;

import java.util.*;

public class BinarySearchTreeImpl<T extends Comparable<T>>
        implements BinarySearchTree<T> {

    class TreeNode {
        T value;
        TreeNode left;
        TreeNode right;

        public TreeNode(T value) {
            this.value = value;
        }
    }

    public TreeNode root;

    @Override
    public void insert(T elem) {
        this.root = insert(this.root, elem);
    }

    private TreeNode insert(TreeNode root, T elem) {
        if (root == null) {
            root = new TreeNode(elem);
        } else {
            if (root.value.compareTo(elem) >= 0) {
                root.left = insert(root.left, elem);
            } else {
                root.right = insert(root.right, elem);
            }
        }
        return root;
    }

    private boolean removeRec(T elem, TreeNode node) {
        TreeNode tn = node;

        if (tn.value.compareTo(elem) == 0 && tn != null) {  // если знач-ие текущей вершины = искомой
            tn.value = null;
            TreeNode bottom = null;
            if (tn.right == null && tn.left == null) {
                tn = null;
                return true;
            } else if (tn.right != null && tn.left == null) {
                tn = tn.right;
                return true;
            } else if (tn.right == null && tn.left != null) {
                tn = tn.left;
                return true;
            } else if (tn.right != null && tn.left != null) {

                TreeNode parentOfBottom = tn.left; // левая вершина удаляемого эл-та (вершина горки)
                if (parentOfBottom.right != null)   // инициализируем нижнюю вершину
                    bottom = parentOfBottom.right;
                while (bottom.right != null) {
                    parentOfBottom = bottom;        // присваиваем родителю значение нижней вершины
                    bottom = parentOfBottom.right;    // крайний правый эл-т (дно)
                }
                if (bottom.left != null)
                    parentOfBottom.right = bottom.left;
                else
                    parentOfBottom.right = null;
                tn.value = bottom.value;
                bottom.value = null;
                // ПРОИЗОШЛО УДАЛЕНИЕ!
                return true;
            }
        }

        if (tn.value.compareTo(elem) < 0 && tn.right != null) { // если знач-ие текущей вершины меньше искомой
            if (tn.right.value.compareTo(elem) == 0) {  // если знач-ие правой вершины = искомой
                tn.right.value = null;
                TreeNode bottom = null;
                if (tn.right.right == null && tn.right.left == null) {  // если у правой вершины нет обоих детей
                    tn.right = null;  // ПРОИЗОШЛО УДАЛЕНИЕ!
                    return true;
                } else if (tn.right.right != null && tn.right.left == null) {  // если у правой вершины нет левого ребенка
                    tn.right = tn.right.right; // ПРОИЗОШЛО УДАЛЕНИЕ!
                    return true;
                } else if (tn.right.right == null && tn.right.left != null) {  // если у правой вершины нет правого ребенка
                    tn.right = tn.right.left;  // ПРОИЗОШЛО УДАЛЕНИЕ!
                    return true;
                } else if (tn.right.right != null && tn.right.left != null) {  // если у правой вершины есть оба ребенка

                    TreeNode parentOfBottom = tn.right.left; // левая вершина удаляемого эл-та (вершина горки)
                    if (parentOfBottom.right != null)   // инициализируем нижнюю вершину
                        bottom = parentOfBottom.right;
                    while (bottom.right != null) {
                        parentOfBottom = bottom;  // присваиваем родителю значение нижней вершины
                        bottom = parentOfBottom.right;    // крайний правый эл-т (дно)
                    }
                    if (bottom.left != null)
                        parentOfBottom.right = bottom.left;
                    else
                        parentOfBottom.right = null;
                    tn.right.value = bottom.value;
                    bottom.value = null;
                    // ПРОИЗОШЛО УДАЛЕНИЕ!
                    return true;
                }
            } else removeRec(elem, tn.right);  // если знач-ие правой вершины != искомой
        }

        if (tn.value.compareTo(elem) > 0 && tn.left != null) { // если значение текущей вершины больше искомой
            if (tn.left.value.compareTo(elem) == 0) {  // если знач-ие левой вершины = искомой
                tn.left.value = null;
                TreeNode bottom = null;
                if (tn.left.right == null && tn.left.left == null) {  // если у левой вершины нет обоих детей
                    tn.left = null;
                    return true;
                } else if (tn.left.right != null && tn.left.left == null) {  // если у левой вершины нет левого ребенка
                    tn.left = tn.left.right;
                    return true;
                } else if (tn.left.right == null && tn.left.left != null) {  // если у левой вершины нет правого ребенка
                    tn.left = tn.left.left;
                    return true;
                } else if (tn.left.right != null && tn.left.left != null) {  // если у левой вершины есть оба ребенка

                    TreeNode parentOfBottom = tn.left.left; // левая вершина удаляемого эл-та (вершина горки)
                    if (parentOfBottom.right != null)   // инициализируем нижнюю вершину
                        bottom = parentOfBottom.right;
                    while (bottom.right != null) {
                        parentOfBottom = bottom;  // присваиваем родителю значение нижней вершины
                        bottom = parentOfBottom.right;    // крайний правый эл-т (дно)
                    }
                    if (bottom.left != null)
                        parentOfBottom.right = bottom.left;
                    else
                        parentOfBottom.right = null;
                    tn.left.value = bottom.value;
                    bottom.value = null;
                    // ПРОИЗОШЛО УДАЛЕНИЕ!
                    return true;

                }
            } else removeRec(elem, tn.left);  // если знач-ие левой вершины != искомой
        }
        return false;
    }

    @Override
    public boolean remove(T elem) {
        // проверяем, есть ли в данном дереве эта вершина
        // если есть, то удаляем ее
        boolean result = false;
        if (contains(elem) && root != null) {
            result = removeRec(elem, root);
        }
        return result;
    }

    @Override
    public boolean contains(T elem) {
        if (root == null)
            return false;
        boolean isChild = false;
        TreeNode tn = root;
        if (tn.value == elem) {
            return true;
        }

        do {
            if (tn.value.compareTo(elem) > 0) {
                if (tn.left == null)
                    return false;
                else {
                    isChild = true;
                    tn = tn.left;
                }
            } else if (tn.value.compareTo(elem) <= 0) {
                if (tn.right == null)
                    return false;
                else {
                    isChild = true;
                    tn = tn.right;
                }
            }

            if (tn.value == elem) {
                return true;
            }

        } while (isChild);

        return false;
    }

    @Override
    public void printAll() {
        printAll(this.root);
    }

    private void printAll(TreeNode root) {
        if (root != null) {
            printAll(root.left);
            System.out.print(root.value + " ");
            printAll(root.right);
        }
    }

    @Override
    public void printAllByLevels() {
        List<List<TreeNode>> levels = traverseLevels(root);

        for (List<TreeNode> level : levels) {
            for (TreeNode node : level) {
                System.out.print(node.value + " ");
            }
            System.out.println();
        }
    }

    private List<List<TreeNode>> traverseLevels(TreeNode root) {
        if (root == null) {
            return Collections.emptyList();
        }
        List<List<TreeNode>> levels = new LinkedList<>();

        Queue<TreeNode> nodes = new LinkedList<>();
        nodes.add(root);

        while (!nodes.isEmpty()) {
            List<TreeNode> level = new ArrayList<>(nodes.size());
            levels.add(level);

            for (TreeNode node : new ArrayList<>(nodes)) {
                level.add(node);
                if (node.left != null) {
                    nodes.add(node.left);
                }
                if (node.right != null) {
                    nodes.add(node.right);
                }
                nodes.poll();
            }
        }
        return levels;
    }
}