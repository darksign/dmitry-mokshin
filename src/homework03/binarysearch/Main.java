package homework03.binarysearch;

public class Main {
    public static void main(String[] args) {
        BinarySearchTree<Integer> bst = new BinarySearchTreeImpl<>();
        bst.insert(1);
        bst.insert(24);
        bst.insert(8);
        bst.insert(26);
        bst.insert(17);
        bst.insert(26);
        bst.insert(10);
        bst.insert(30);
        bst.remove(30);
        System.out.println(bst.contains(30));
        System.out.println(bst.contains(10));
        bst.printAll();
        System.out.println();
        System.out.println();
        bst.printAllByLevels();
    }
}