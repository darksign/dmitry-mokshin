package homework03.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Reflection {
    public static <T> List<T> getMany(Class<T> c, int count,
                                      Object... args) {
        List<T> arrayList = new ArrayList<>();
        try {
            Class[] a = Arrays.stream(args).map(o -> o.getClass()).toArray(Class[]::new);
            Constructor b = c.getConstructor(a);
            for (int i = 0; i < count; i++) {
                arrayList.add((T) b.newInstance(args));
            }

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return arrayList;
    }
}
