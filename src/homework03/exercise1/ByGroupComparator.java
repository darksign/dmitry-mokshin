package homework03.exercise1;

import java.util.Comparator;

public class ByGroupComparator implements Comparator<Student>{
    @Override
    public int compare(Student o1, Student o2) {
        //если o1 больше о2, то вернет число > 0
        //если o1 меньше о2, то вернет число < 0
        //если o1 равен о2, то вернет 0
        return o1.getGroup() - o2.getGroup();
    }
}