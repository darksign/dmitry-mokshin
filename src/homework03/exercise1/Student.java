package homework03.exercise1;

public class Student implements Comparable<Student>{
    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    private String first_name;
    private String last_name;
    private int group;

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    private int rating;

    public Student(String first_name, String last_name, int group, int rating) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.group = group;
        this.rating = rating;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Student)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Student st = (Student) obj;
        return this.getFirst_name().equals(st.getFirst_name()) &&
                this.getLast_name().equals(st.getLast_name()) &&
                this.getGroup() == st.getGroup();
    }

    @Override
    public int compareTo(Student o) {
        return this.getLast_name().compareTo(o.getLast_name());
    }
}