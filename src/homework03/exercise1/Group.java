package homework03.exercise1;

import java.util.Comparator;

public class Group {
    private final static int INITIAL_CAPACITY = 10;
    Student[] StudentsArray;
    private int n;
    Comparator<Student> comparator;

    public Group() {
        StudentsArray = new Student[INITIAL_CAPACITY];
        this.n = 0;
    }

    public Group(Comparator<Student> comparator) {
        this();
        this.comparator = comparator;
    }

    public void add(Student st) {
        int i = 0;
        while (i < n) {
            if (comparator != null) {
                if (comparator.compare(st, StudentsArray[i]) < 0) {
                    break;
                }
            } else {
                if (st.compareTo(StudentsArray[i]) < 0) {
                    break;
                }
            }
            i++;
        }
        for (int j = n; j > i; j--) {
            StudentsArray[j] = StudentsArray[j - 1];
        }
        StudentsArray[i] = st;
        n++;
    }
}