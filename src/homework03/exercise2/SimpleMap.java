package homework03.exercise2;

public class SimpleMap<K, V> implements Map<K, V> {
    private static final int SIZE = 1;
    public Entry<K, V>[] entries;
    private int n;


    public SimpleMap() {
        this.entries = new Entry[SIZE];
        this.n = 0;
    }

    @Override
    public void put(K key, V value) {
        for (int i = 0; i < n; i++) {
            if (entries[i].key.equals(key)) {
                entries[i].value = value;
                return;
            }
        }
        if (n == entries.length) {
            Entry<K,V>[] newArray = new Entry[entries.length+1];
            for (int i = 0; i < entries.length; i++) {
                newArray[i] = entries[i];
            }
            entries = newArray;
        }
        entries[n++] = new Entry<>(key, value);
    }

    @Override
    public V get(K key) {
        for (int i = 0; i < n; i++) {
            if (entries[i].key.equals(key)) {
                return (V)entries[i].value;
            }
        }
        return null;
    }

    class Entry<I, O> {
        I key;
        O value;

        public Entry(I key, O value) {
            this.key = key;
            this.value = value;
        }
    }
}
