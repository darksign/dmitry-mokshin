package homework03.exercise2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        File f = new File("input.txt");
        Scanner in = new Scanner(f);
        Map<String, Integer> m = new SimpleMap<>();

        while (in.hasNext()) {
            String input = in.next();
            if (m.get(input) != null) {
                m.put(input, m.get(input) + 1);
            } else {
                m.put(input, 1);
            }
        }
        for (int i = 0; i < ((SimpleMap<String, Integer>) m).entries.length; i++) {
            if(((SimpleMap<String, Integer>) m).entries[i] != null)
            System.out.println(((SimpleMap<String, Integer>) m).entries[i].key + " " + ((SimpleMap<String, Integer>) m).entries[i].value);
        }
    }
}