package additional_tasks;

public class Task007 {
    public static void main(String[] args) {
        double a = 2;
        double b = 3;

        System.out.println("a + b = " + (a + b));
        System.out.println("a - b = " + (a - b));
        System.out.println("b - a = " + (b - a));
        System.out.println("a * b = " + (a * b));
        System.out.println("a / b = " + (a / b));
        System.out.println("a % b = " + (a % b));
        System.out.println("b / a = " + (b / a));
        System.out.println("b % a = " + (b % a));
    }
}
