package task13;

public class InterfaceDemo {
    public static void main(String[] args) {
        Point point = new Point();
        Unit unit = new Unit();
        Exploiter exploiter = new Exploiter();

        exploiter.setCoords(point, 2, 3);
        exploiter.setCoords(unit, 2, 3);
        exploiter.printInf(point);
        exploiter.printInf(unit);
        exploiter.relocate(point, 6, 4);
        exploiter.relocate(unit, 5, -5);
        exploiter.setColor(point, "green");
        exploiter.setColor(unit, "red");
        exploiter.printInf(point);
        exploiter.printInf(unit);


    }
}
