package task13;

public interface Moveable {
    void moveX(int x);
    void moveY(int y);
}
