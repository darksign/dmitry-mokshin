package task13;

public class Point implements Moveable, Placeable, Printable, Colorable {

    public int getX() {
        return x;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    public int x = 0;

    public int getY() {
        return y;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    public int y = 0;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String color;

    public void moveX(int x){
        this.x = this.x + x;
    }

    public void moveY(int y){
        this.y = this.y + y;
    }

    public void print(){
        System.out.println("Point coordinates: " + getX() + " " + getY() + "\n" + "Point color: " + getColor());
    }
}