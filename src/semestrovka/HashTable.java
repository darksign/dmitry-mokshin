package semestrovka;

class HashTable {
    private DataItem[] hashArray; // Массив ячеек хеш-таблицы

    public int getArraySize() {
        return arraySize;
    }

    private int arraySize;
    private DataItem nonItem; // Для удаленных элементов

    HashTable(int size) {
        int primeSize;
        if (!isPrime(size)) {  // если длина не простое число
            primeSize = getPrime(size);   // создаем простое число, следующее после него
            arraySize = primeSize;
        }
        else arraySize = size;
        hashArray = new DataItem[arraySize];
        nonItem = new DataItem(-1);
    }

    public void displayTable() {
        System.out.print("Table: ");
        for (int j = 0; j < arraySize; j++) {
            if (hashArray[j] != null)
                System.out.print(hashArray[j].getKey() + " ");
            else
                System.out.print("** ");
        }
        System.out.println();
    }

    public int hashFunc1(int key) {
        return key % arraySize;
    }

    public int hashFunc2(int key) {
// Возвращаемое значение отлично от нуля, меньше размера массива,
// функция отлична от хеш-функции 1
// Размер массива должен быть простым по отношению к 5, 4, 3 и 2
        return 5 - key % 5;
    }

    // Вставка элемента данных
    public void insert(int key, DataItem item) {
// (Метод предполагает, что таблица не заполнена)
        int hashVal = hashFunc1(key); // Хеширование ключа
        int stepSize = hashFunc2(key); // Вычисление смещения
// Пока не будет найдена
        while (hashArray[hashVal] != null &&            // пустая ячейка или -1
                hashArray[hashVal].getKey() != -1 && hashArray[hashVal].getKey() != key) {
            hashVal += stepSize; // Прибавление смещения
            hashVal %= arraySize; // Возврат к началу
        }
        hashArray[hashVal] = item; // Вставка элемента
    }

    public DataItem delete(int key) {  // Удаление элемента данных
        int hashVal = hashFunc1(key); // Хеширование ключа
        int stepSize = hashFunc2(key); // Вычисление смещения
        while(hashArray[hashVal] != null) {  // Пока не найдена пустая ячейка
         // Ключ найден?
            if(hashArray[hashVal].getKey() == key) {
                DataItem temp = hashArray[hashVal]; // Временное сохранение
                hashArray[hashVal] = nonItem; // Удаление элемента
                return temp; // Метод возвращает элемент
            }
            hashVal += stepSize; // Прибавление смещения
            hashVal %= arraySize; // Возврат к началу
        }
        return null; // Элемент не найден
    }

    public DataItem find(int key) {    // Поиск элемента с заданным ключом
// (Метод предполагает, что таблица не заполнена)
        int hashVal = hashFunc1(key); // Хеширование ключа
        int stepSize = hashFunc2(key); // Вычисление смещения
        while(hashArray[hashVal] != null) {    // Пока не найдена пустая ячейка
         // Ключ найден?
            if(hashArray[hashVal].getKey() == key)
                return hashArray[hashVal]; // Да, метод возвращает элемент
            hashVal += stepSize; // Прибавление смещения
            hashVal %= arraySize; // Возврат к началу
        }
        return null; // Элемент не найден
    }

    private int getPrime(int min) {         // Возвращает первое простое число > min
        for(int j = min+1; true; j++) // Для всех j > min
            if( isPrime(j) ) // Число j простое?
                return j; // Да, вернуть его
    }

    private boolean isPrime(int n) {        // Число n простое?
        for(int j=2; (j*j <= n); j++) // Для всех j
            if( n % j == 0) // Делится на j без остатка?
                return false; // Да, число не простое
        return true; // Нет, число простое
    }
}