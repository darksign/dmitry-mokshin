package semestrovka;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        // создание данных и занесение их в файл
        File file = new File("semdata.txt");
        FileReader fr = new FileReader(file);
        Scanner sc = new Scanner(fr);
        int nabor = 1;

        while (sc.hasNext()) {
            String input = sc.nextLine();
            String[] strings = input.split(" ");
            int arraySize = strings.length * 2;
            HashTable hashTable = new HashTable(arraySize);
            for (int i = 0; i < strings.length; i++) {
                int key = Integer.parseInt(strings[i]);
                DataItem di = new DataItem(key);
                if (i == 20 || i == 30 || i == 40) {
                    System.out.println("Начало вставки " + nabor + "-ый набор: " + System.nanoTime());
                    hashTable.insert(key, di);
                    System.out.println("Окончание вставки " + nabor + "-ый набор: " + System.nanoTime());
                    System.out.println("Начало поиска " + nabor + "-ый набор: " + System.nanoTime());
                    hashTable.find(key);
                    System.out.println("Окончание поиска " + nabor + "-ый набор: " + System.nanoTime());
                    System.out.println("Начало удаления " + nabor + "-ый набор: " + System.nanoTime());
                    hashTable.delete(key);
                    System.out.println("Окончание удаления " + nabor + "-ый набор: " + System.nanoTime());
                } else hashTable.insert(key, di);
            }
            hashTable.displayTable();
            System.out.println("_____________________________________________________");
            System.out.println();
            nabor = nabor + 1;
        }
    }
}