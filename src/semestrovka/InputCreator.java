package semestrovka;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class InputCreator {
    String[] inputStrings;
    File file;
    FileWriter fw;
    BufferedWriter bw;

    public InputCreator() throws IOException {
        file = new File("semdata.txt");
        fw = new FileWriter(file);
        bw = new BufferedWriter(fw);
    }

    public void generateData(int n) {  // n - кол-во слов
        String inputData = "";
        Random r = new Random();
        int wordLength;
        int random;
        for (int i = 0; i < n; i++) {
            wordLength = r.nextInt(4) + 2; // длина слова от 2 до 5
            for (int j = 0; j < wordLength; j++) {
                random = (r.nextInt(8) + 1);
                inputData = inputData + random;
            }
            inputData += " ";
        }
        inputStrings = inputData.split(" ");
    }

    public void writeData() throws IOException {
        for (int i = 0; i < inputStrings.length; i++) {
            bw.write(inputStrings[i]);
            bw.write(" ");
        }
        bw.flush();
        bw.write("\n");
    }

    public void closeBW() throws IOException {
        bw.close();
    }

    public static void main(String[] args) throws IOException { // записываем данные в файл
        InputCreator IC = new InputCreator();
        int numberOfElement = 0;
        for (int i = 0; i < 100; i++) {
            numberOfElement += 100;
            IC.generateData(numberOfElement);
            IC.writeData();
        }
        IC.closeBW();
    }
}