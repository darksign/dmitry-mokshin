package semestrovka;

class DataItem {
    private int iData; // Данные (ключ)

    public DataItem(int ii) {
        iData = ii;
    }

    public int getKey() {
        return iData;
    }
}