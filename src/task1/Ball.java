package task1;

public class Ball {
    private double volume;
    private double diam;

    public double getDiam() {
        return diam;
    }
    public double getVolume() {
        return volume;
    }
    public Ball(double diam) {
        this.diam = diam;
    }
    public void VolumeB() {
        volume = 4 * 3.14 * Math.pow(diam, 3) / 24;
    }
}
