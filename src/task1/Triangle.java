package task1;

public class Triangle {
    private double k1;
    private double k2;
    private double angle;
    private double angleR;
    private double area;

    public double getK1() {
        return k1;
    }
    public double getK2() {
        return k2;
    }
    public double getAngle() {
        return angle;
    }
    public double getArea() {
        return area;
    }
    public Triangle(double k1, double k2) {
        this.k1 = k1;
        this.k2 = k2;
        this.angle = angle;
    }
    public void setAngle(double angle) {
        this.angle = angle;
        angleR = Math.toRadians(angle);

    }
    public void tArea() {
        area = k1 * k2 * Math.sin(angleR) / 2;
    }
}
