package task1;

public class Circle {
    private double diam;
    private double circom; // Площадь круга
    private double secArea; // Площадь сектора

    public void setCircom(double circom) {
        this.circom = circom;
    }

    public Circle(double diam) {
        this.diam = diam;
    }

    public double getCircom() {
        return circom;
    }

    public double getDiam() {
        return diam;
    }

    public void cArea(double x) {
        setCircom(3.14 * x * x / 4);
    }
    public void secArea(double angle, double diam) {
        secArea = 1 / 2 * Math.pow(diam / 2, 2) * (angle * 3.14 / 180 - Math.sin(angle));
    }
}
