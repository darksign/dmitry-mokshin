package task1;

public class window {

    private boolean isLocked = false;


    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        if (locked && isLocked) {
            System.out.println("Door is already closed");
            return;
        }
        isLocked = locked;
    }
}
