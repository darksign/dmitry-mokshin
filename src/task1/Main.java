package task1;
import java.util.Scanner;

public class Main {

    public static void main(String args[]) {

        Scanner sc = new Scanner(System.in);

        System.out.println("What do you want to calculate?");
        System.out.println("Select number:");
        System.out.println("1) Area of the circle");
        System.out.println("2) Area of the triangle");
        System.out.println("3) Pyramid volume");
        System.out.println("4) Ball volume");
        int n = sc.nextInt();
        if (n > 4 || n < 1) {
            System.out.println("Invalid number");
        }
        else {
            switch (n) {
                case 1:
                    System.out.println("Enter the diameter");
                    Circle c1 = new Circle(sc.nextDouble());
                    System.out.println("Diameter is " + c1.getDiam());
                    c1.cArea(c1.getDiam());
                    System.out.println("Area of the circle equal to " + c1.getCircom());
                    break;
                case 2:
                    System.out.println("Enter the two sides");
                    Triangle t1 = new Triangle(sc.nextDouble(), sc.nextDouble());
                    System.out.println("Enter the angle between these sides");
                    t1.setAngle(sc.nextDouble());
                    t1.tArea();
                    System.out.println("Area of the triangle equal to " + t1.getArea());
                    break;
                case 3:
                    System.out.println("Enter the height and base area");
                    Piram p1 = new Piram(sc.nextDouble(), sc.nextDouble());
                    p1.Volume();
                    System.out.println("Pyramid volume equal to " + p1.getVolume());
                    break;
                case 4:
                    System.out.println("Enter the diameter");
                    Ball b1 = new Ball(sc.nextDouble());
                    b1.VolumeB();
                    System.out.println("Ball volume equal to " + b1.getVolume());
                    break;
            }
        }
    }
}