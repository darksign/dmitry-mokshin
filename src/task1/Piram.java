package task1;

public class Piram {
    private double height;
    private double area;
    double volume = 0;

    public double getHeight() {
        return height;
    }
    public double getArea() {
        return area;
    }
    public double getVolume() {
        return volume;
    }
    public Piram(double height, double area) {
        this.height = height;
        this.area = area;
    }
    public double Volume() {
        volume = height * area / 3;
        return volume;
    }
}
