package stackv2;

public class StackTestMain {
    public static void main(String[] args) {
        StackV2 s1 = new StackV2();
        StackV2 s2 = new StackV2();
        s1.push('1');
        s1.push('2');
        System.out.println(s1.pop());
        System.out.println(s1.pop());
    }
}