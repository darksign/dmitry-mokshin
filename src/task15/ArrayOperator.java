package task15;
// Consists methods using arrays
public class ArrayOperator {
    public Array array;
    private ArrayDisplay ad;

    public ArrayOperator(Array array, ArrayDisplay ad) {
        this.array = array;
        this.ad = ad;
    }

    public boolean isEquals(Array a, Array b) { // checks lengths of arrays
        int al = 0;
        int bl = 0;
        for (int i = 0; i < a.getFieldLength(); i++)
            al += 1;
        for (int i = 0; i < b.getFieldLength(); i++)
            bl += 1;
        if (al != bl)
            return false;
        else return true;
    }

    public void join(Array a, Array b) {   // combines asterisks of two arrays
        if (isEquals(a, b)) {
            for (int i = 0; i < b.getFieldLength(); i++)
                for (int j = 0; j < b.getFieldLength(); j++)
                    if (a.getCell(i, j) != b.getCell(i, j) || a.getCell(i,j) == a.getAsterisk() && b.getCell(i,j) == b.getAsterisk())
                        array.setAsterisk(i, j);
            ad.display(array);
            cleanArray();
        }
        else
            System.out.println("Lengths are unequal!");
    }

    public void intersect(Array a, Array b) {     // finds mutual asterisks
        if (isEquals(a, b)){
            for (int i = 0; i < a.getFieldLength(); i++)
                for (int j = 0; j < a.getFieldLength(); j++)
                    if (a.getCell(i,j) == a.getAsterisk() && b.getCell(i,j) == b.getAsterisk())
                        array.setAsterisk(i,j);
            ad.display(array);
            cleanArray();
        }
        else
            System.out.println("Lengths are unequal!");
    }

    public void cleanArray() {              // cleans array after using
        for (int i = 0; i < array.getFieldLength(); i++)
            for (int j = 0; j < array.getFieldLength(); j++)
                array.setEmptyCell(i,j);
    }
}