package task15;
// Keeps data of a field
public class Array {
    private char[][] field = new char[3][3]; // square field!
    private char asterisk = '*';

    public Array() {
        for (int i = 0; i < field.length; i++)
            for (int j = 0; j < field[0].length; j++)
                setEmptyCell(i, j);
    }

    public void setAsterisk(int x, int y){
        field[x][y] = asterisk;
    }

    public void setEmptyCell(int x, int y){
        field[x][y] = ' ';
    }

    public void createArray1() {          // create first type of a field
        for (int i = 0; i < field.length; i++)
            setAsterisk(1, i);
    }

    public void createArray2() {         // create second type of a field
        for (int i = 0; i < field.length; i++)
            setAsterisk(i, 1);
    }

    public int getFieldLength() {
        return field.length;
    }

    public char getCell(int x, int y) {
        return field[x][y];
    }

    public char getAsterisk() {
        return asterisk;
    }
}