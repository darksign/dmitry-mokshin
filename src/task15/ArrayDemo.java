package task15;

public class ArrayDemo {
    public static void main(String[] args) {
        Array a = new Array();
        Array b = new Array();
        Array c = new Array();
        a.createArray1();
        b.createArray2();
        ArrayDisplay ad = new ArrayDisplay();
        ad.display(a);
        System.out.println();
        ad.display(b);
        System.out.println();
        ArrayOperator ao = new ArrayOperator(c, ad);
        ao.join(a, b);
        System.out.println();
        ao.intersect(a, b);
    }
}