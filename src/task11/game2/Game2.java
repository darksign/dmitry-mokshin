package task11.game2;

import java.util.Scanner;

public class Game2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Field f = new Field();
        boolean run = !f.isEnd();
        do {
            for(int i = 0; i < f.getFieldLength(); i++) {
                f.printField();
                System.out.println("Введите координаты крестика (по гор. и верт.)");
                int x = in.nextInt();
                int y = in.nextInt();
                while(!f.x(x, y)) {
                    System.out.println("Введите координаты крестика (по гор. и верт.)");
                    x = in.nextInt();
                    y = in.nextInt();
                }
                System.out.println();
                f.printField();
                System.out.println();
                if (f.getFilledFields() == 9) {
                    run = false;
                    break;
                }
                if (f.isEnd()) {
                    run = false;
                    break;
                }
                System.out.println("Введите координаты нолика (по гор. и верт.)");
                x = in.nextInt();
                y = in.nextInt();
                while(!f.o(x, y)) {
                    System.out.println("Введите координаты крестика (по гор. и верт.)");
                    x = in.nextInt();
                    y = in.nextInt();
                }
                System.out.println();
                f.printField();
                System.out.println();
                if (f.getFilledFields() == 9) {
                    run = false;
                    break;
                }
                if (f.isEnd()) {
                    run = false;
                    break;
                }
            }
        } while (run);
        System.out.println("Game over!");
    }
}