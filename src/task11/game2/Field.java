package task11.game2;

public class Field {
    private String[][] field = new String[3][3];
    private int filledFields = 0;


    public Field() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field[i][j] = " ";
            }
        }
    }

    public int getFieldLength() {
        return field.length;
    }

    public int getFilledFields() {
        return filledFields;
    }

    public boolean x(int x, int y) {
        if (!field[x][y].equals("x") && !field[x][y].equals("0")) {
            field[x][y] = "x";
            filledFields += 1;
            return true;
        }
        else return false;
    }

    public boolean o(int x, int y) {
        if (!field[x][y].equals("x") && !field[x][y].equals("0")) {
            field[x][y] = "0";
            filledFields += 1;
            return true;
        }
        else return false;
    }
    
    public void printField() {
        System.out.println("  012");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < 3; j++) {
                System.out.print(field[i][j]);
            }
            System.out.println();
        }
    }
    
    public boolean isEnd() {
        String s = field[0][0];
        if (s.equals(field[1][1]) && s.equals(field[2][2]) && !s.equals(" "))
            return true;
        s = field[0][2];
        if (s.equals(field[1][1]) && s.equals(field[2][0]) && !s.equals(" "))
            return true;
        s = field[0][0];
        if (s.equals(field[0][1]) && s.equals(field[0][2]) && !s.equals(" "))
            return true;
        s = field[1][0];
        if (s.equals(field[1][1]) && s.equals(field[1][2]) && !s.equals(" "))
            return true;
        s = field[2][0];
        if (s.equals(field[2][1]) && s.equals(field[2][2]) && !s.equals(" "))
            return true;
        s = field[0][0];
        if (s.equals(field[1][0]) && s.equals(field[2][0]) && !s.equals(" "))
            return true;
        s = field[0][1];
        if (s.equals(field[1][1]) && s.equals(field[2][1]) && !s.equals(" "))
            return true;
        s = field[0][2];
        if (s.equals(field[1][2]) && s.equals(field[2][2]) && !s.equals(" "))
            return true;
        return false;
    }
}