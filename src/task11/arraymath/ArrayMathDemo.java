package task11.arraymath;

public class ArrayMathDemo {
    public static void main(String[] args) {
        int[] nums = {5, 7, 8, 10, 15, 2, 3};
        ArrayMath a = new ArrayMath(nums);
        System.out.println(a.sum());
        System.out.println(a.avg());
        System.out.println(a.min());
        System.out.println(a.max());
    }
}