package task11.arraymath;

public class ArrayMath {
    private int[] nums;

    public ArrayMath(int[] nums) {
        this.nums = nums;
    }

    public double sum() {
        double sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum = sum + nums[i];
        }
        return sum;
    }

    public double avg() {
        double avg;
        avg = sum() / nums.length;
        return avg;
    }

    public int min() {
        int min = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] < min)
                min = nums[i];
        }
        return min;
    }

    public int max() {
        int max = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] > max)
                max = nums[i];
        }
        return max;
    }
}