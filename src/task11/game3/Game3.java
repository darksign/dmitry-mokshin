package task11.game3;

import java.util.Random;
import java.util.Scanner;

public class Game3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        String[] vars = {"Камень", "Ножницы", "Бумага"};
        boolean run = true;
        System.out.println("Вы играете в \"камень, ножницы, бумага\"" + "\n" + "Чтобы победить, вы должны выбрать вариант, который победит компьютер");
        do {
            int randomNum = rand.nextInt(3);
            String comp = vars[randomNum];
            System.out.println("Выберите камень, ножницы или бумагу(0, 1, 2 соответственно)");
            int userNum = in.nextInt();
            String user = vars[userNum];
            System.out.println(user + " vs " + comp);
            if (user.equals(vars[0]) && comp.equals(vars[2])) {    // камень и бумага
                System.out.println("Вы проиграли!");
                run = false;
            }
            if (user.equals(vars[2]) && comp.equals(vars[0])){
                System.out.println("Вы победили!");
                run = false;
            }
            if (user.equals(vars[2]) && comp.equals(vars[1])) {   // ножн и бум
                System.out.println("Вы проиграли!");
                run = false;
            }
            if (user.equals(vars[1]) && comp.equals(vars[2])){
                System.out.println("Вы победили!");
                run = false;
            }
            if (user.equals(vars[1]) && comp.equals(vars[0])){   // ножн и камень
                System.out.println("Вы проиграли!");
                run = false;
            }
            if (user.equals(vars[0]) && comp.equals(vars[1])){
                System.out.println("Вы победили!");
                run = false;
            }
        } while (run);
    }
}