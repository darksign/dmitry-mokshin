package task11.game1;

import java.util.Random;

public class Field {
    private String[][] field = new String[4][4];  // квадратная
    private String[] chars = {"#", "@", "!", "3"};

    public int getFieldLength() {
        return field[0].length;
    }

    public Field() {                           // создание пустого поля
        for (int i = 0; i < field[0].length; i++) {
            for (int j = 0; j < field[0].length; j++)
                field[i][j] = " ";
        }
    }

    public void setFill(Field f, int x, int y) {   //перенос знач-ия ячейки из запол-го поля в пустое
        field[x][y] = f.field[x][y];
    }

    public String getFill(int x, int y) {
        return field[x][y];
    }

    public void resetFill(int x, int y) {          // сброс символа
        field[x][y] = " ";
    }

    public void setField() {                       //заполнение поля символами в случайном порядке
        setLattice();
        setDog();
        setExclMark();
        setThree();
    }

    public void setLattice() {
        final Random rand = new Random();
        int amount = field.length * field[0].length / 4;
        int c = 0;
        while (c < amount) {
            int x = (rand.nextInt(4));
            int y = (rand.nextInt(4));
            if (field[x][y].equals(" ")) {
                field[x][y] = chars[0];
                c++;
            }
        }
    }

    public void setDog() {
        final Random rand = new Random();
        int amount = field.length * field[0].length / 4;
        int c = 0;
        while (c < amount) {
            int x = (rand.nextInt(4));
            int y = (rand.nextInt(4));
            if (field[x][y].equals(" ")) {
                field[x][y] = chars[1];
                c++;
            }
        }
    }

    public void setExclMark() {
        final Random rand = new Random();
        int amount = field.length * field[0].length / 4;
        int c = 0;
        while (c < amount) {
            int x = (rand.nextInt(4));
            int y = (rand.nextInt(4));
            if (field[x][y].equals(" ")) {
                field[x][y] = chars[2];
                c++;
            }
        }
    }

    public void setThree() {
        final Random rand = new Random();
        int amount = field.length * field[0].length / 4;
        int c = 0;
        while (c < amount) {
            int x = (rand.nextInt(4));
            int y = (rand.nextInt(4));
            if (field[x][y].equals(" ")) {
                field[x][y] = chars[3];
                c++;
            }
        }
    }

    public boolean checkFills() {           //проверка на заполненность
        int fills = 0;
        for (int i = 0; i < field.length; i++)
            for (int j = 0; j < field[0].length; j++)
                if (!field[i][j].equals(" "))
                    fills++;
                else continue;
        if (fills == field.length * field[0].length)
            return false;
        else return true;
    }
}
