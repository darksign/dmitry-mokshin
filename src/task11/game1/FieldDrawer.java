package task11.game1;

public class FieldDrawer {

    public void fieldDraw(Field f) {
        System.out.print("   ");
        for (int y = 0; y < f.getFieldLength(); y++)
            System.out.print(y + " ");
        System.out.println();
        for (int i = 97; i < 97 + f.getFieldLength(); i++) {
            char k = (char) i;
            System.out.print(k + " |");
            for (int j = 0; j < f.getFieldLength(); j++)
                System.out.print(f.getFill((int) i - 97, j) + "|");
            System.out.println();
        }
    }
}