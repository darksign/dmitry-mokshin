package task11.game1;

import java.util.Scanner;

public class Game1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Field f = new Field();
        f.setField();                      // ввод символов в поля
        Field femp = new Field();              // создаются 2 поля
        FieldDrawer fd = new FieldDrawer();    // пустое и заполненное
        fd.fieldDraw(femp);
        do {
            System.out.println("Введите поле, которое хотите открыть(по гор. и верт.):");
            String a = in.next();
            char b = a.charAt(0);
            int x = b - 97;           //чтобы можно было вводить по координатам на поле
            int y = in.nextInt();
            femp.setFill(f, x, y);    // берем значение из запол.поля и переносим на пустое
            String first = f.getFill(x, y);     //запоминаем этот символ
            fd.fieldDraw(femp);
            System.out.println("Введите второе поле, которое хотите открыть(по гор. и верт.):");
            a = in.next();
            b = a.charAt(0);
            int z = b - 97;
            int w = in.nextInt();
            if (f.getFill(z, w).equals(first)) {   // если символы совпали
                femp.setFill(f, z, w);
                fd.fieldDraw(femp);
            } else {                               //если не совпали
                femp.setFill(f, z, w);
                fd.fieldDraw(femp);
                System.out.println("Неверно!");
                femp.resetFill(x, y);
                femp.resetFill(z, w);
                fd.fieldDraw(femp);
            }
        } while (femp.checkFills());     //пока поле не заполнится
        if (femp.checkFills())
            System.out.println("Вы молодец!");
    }
}