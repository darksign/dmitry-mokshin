package task7;

public class Task3 {

    public static void main(String[] args) {
        float[] num = new float[args.length];
        for (int i = 0; i < num.length; i++) {
            float t = Float.parseFloat(args[i]);
            num[i] = t;
        }

        float result = sum(num);
        System.out.printf("%.2f", result);
    }

    public static float sum(float args[]) {
        float sum = 0.0f;
        for(int i=0;i<args.length;i++){
            sum += args[i];
        }
        return sum;
    }


}
