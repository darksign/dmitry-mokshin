package task7;

import java.util.Arrays;

public class Task6 {

    public static void main(String[] args) {
        char[] a = new char[args[0].length()];
        char[] b = new char[args[1].length()];

        for (int i = 0; i < args[0].length(); i++) {
            a[i] = args[0].charAt(i);
        }

        for (int i = 0; i < args[1].length(); i++) {
            b[i] = args[1].charAt(i);
        }

        Arrays.sort(a);
        Arrays.sort(b);
        System.out.println(result(a, b));


    }

    public static boolean result(char[] a, char[] b) {
        if (Arrays.equals(a, b) )
            return true;
        else
            return false;
    }
}

