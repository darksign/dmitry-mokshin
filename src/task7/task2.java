package task7;

public class task2 {

    public static void main(String[] args) {
        int num[] = new int[args.length];
        for(int i = 0;i < num.length;i++){
            int t = Integer.parseInt(args[i]);
            num[i] = t;
        }
        int result = sum(num);
        System.out.println(result);
    }

    public static int sum(int args[]){
        int sum = 0;
        for(int i=0;i<args.length;i++){
            sum += args[i];
        }
        return sum;
    }
}