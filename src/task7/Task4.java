package task7;

public class Task4 {

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        char b = args[1].charAt(0);
        int c = Integer.parseInt(args[2]);

        switch (b) {
            case '+':
                System.out.println(a+c);
                break;
            case '-':
                System.out.println(a-c);
                break;
            case '/':
                System.out.println(a/c);
                break;
            case '*':
                System.out.println(a*c);
                break;
        }
    }
}
