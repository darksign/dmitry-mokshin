package homework01.intlist;

public interface IntIterator {
    boolean hasNext();
    int next();
}