package homework01.intlist;

public class ArrayIntList implements IntList{
    private final static int INITIAL_CAPACITY = 10;
    private final static double COEFFICIENT = 1.5;
    private int[] arr;
    private int n;

    public ArrayIntList(){
        arr = new int[INITIAL_CAPACITY];
        n = 0;
    }

    @Override
    public void add(int elem) {
        if (n == arr.length) {
            int[] newArray = new int[(int) (arr.length * COEFFICIENT)];
            for (int i = 0; i < arr.length; i++) {
                newArray[i] = arr[i];
            }
            arr = newArray;
        }
        arr[n++] = elem;
    }

    @Override
    public void add(int elem, int position) {
        if (position >= arr.length || position < 0)
            throw new IllegalStateException("Слишком большая/маленькая позиция");
        if (n == arr.length) {
            int[] newArray = new int[(int) (arr.length * COEFFICIENT)];
            for (int i = 0; i < position; i++) {
                newArray[i] = arr[i];
            }
            newArray[position] = elem;
            for (int i = position + 1; i <= n; i++) {
                newArray[i] = arr[i - 1];
            }
            arr = newArray;
            n++;
        } else {
            for (int i = position + 1; i <= arr.length; i++) {  // ???
                arr[i] = arr[i - 1];
            }
            arr[position] = elem;
            n++;
        }
    }

    @Override
    public int get(int index) {
        if (index >= arr.length || index < 0)
            throw new IllegalStateException("Неверно указан индекс");
        return arr[index];
    }

    @Override
    public int remove(int index) {
        if (index >= arr.length || index < 0)
            throw new IllegalStateException("Неверно указан индекс");
        int r = arr[index];
        arr[index] = 0;
        for (int i = index; i < arr.length-1; i++) {
            arr[i] = arr[i+1];
        }
        n--;
        return r;

    }

    @Override
    public int size() {
        return n;
    }

    @Override
    public int[] toArray() {
        return arr;
    }

    @Override
    public void sort() {
        for (int out = n - 1; out >= 1; out--){
            for (int in = 0; in < out; in++){
                if(arr[in] > arr[in + 1])
                    toSwap(in, in + 1);
            }
        }
    }

    @Override
    public void addAll(IntList list, int position) {
        if (list.size() > n)
            throw new IllegalStateException("Слишком большой лист");
        for (int i = 0; i < list.size(); i++) {
            add(list.get(i), position + i);      // ???
        }
    }

    @Override
    public int lastIndexOf(int elem) {
        for (int i = arr.length-1; i >= 0; i--) {
            if (arr[i] == elem)
                return i;
        }
        throw new IllegalStateException("Число не найдено");
    }

    // вспомогательный метод для сортировки
    private void toSwap(int first, int second){
        int dummy = arr[first];
        arr[first] = arr[second];
        arr[second] = dummy;
    }

    @Override
    public IntIterator iterator() {
        return new IteratorImpl();
    }

    class IteratorImpl implements IntIterator {
        private int currentIndex;

        public IteratorImpl() {
            currentIndex = 0;
        }

        @Override
        public int next() {
            return get(currentIndex++);
        }

        @Override
        public boolean hasNext() {
            return currentIndex < n;
        }
    }
}