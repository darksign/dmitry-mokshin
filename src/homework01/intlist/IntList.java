package homework01.intlist;

public interface IntList {
    /**
     * Adds number to the end of the list
     * @param elem number to add
     */
    void add(int elem);

    void add(int elem, int position);

    /**
     * Returns the element with specified index
     */
    int get(int index);

    // возвращает удаленный эл-т
    int remove(int index);

    // возвращает кол-во эл-ов в списке
    int size();

    // возвращает сод-ое списка в виде массива
    int[] toArray();

    // упорядочивает значения в списке по возрастанию
    void sort();

    // вставляет в данный список все эл-ты из List. начиная
    // с position в данном списке
    void addAll(IntList list, int position);

    // индекс последнего вхождения данного элемента в списке
    int lastIndexOf(int elem);

    IntIterator iterator();
}