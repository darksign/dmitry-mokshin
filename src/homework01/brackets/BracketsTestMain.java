package homework01.brackets;

import java.util.Scanner;

public class BracketsTestMain {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        boolean b = areBracketsCorrect(s);
        System.out.println(b);
    }

    static boolean areBracketsCorrect(String str){
        Stack stack = new Stack(100);
        char[] symbols = str.toCharArray();
        for (char c: symbols) {
            if (c == '(' || c == '[' || c == '{') {
                try {
                    stack.push(c);
                } catch(IllegalStateException e) {
                    return false;
                }
            } else {
                try {
                    if (c == ')') {
                        if (stack.pop() != '(')
                            return false;
                    }
                    if (c == ']') {
                        if (stack.pop() != '[')
                            return false;
                    }
                    if (c == '}') {
                        if (stack.pop() != '{')
                            return false;
                    }
                } catch (IllegalStateException e) {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}