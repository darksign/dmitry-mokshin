package homework01.brackets;

public class Stack {
    char[] arr;
    int n;

    public Stack(int capacity){
        arr = new char[capacity];
        n = 0;
    }

    void push(char c) throws IllegalStateException {
        if(n == arr.length) {
            throw new IllegalStateException("Stack is already full");
        }
        arr[n++] = c;
    }

    char pop() throws IllegalStateException {
        if (n == 0){
            throw new IllegalStateException("Stack is empty");
        }
        return arr[--n];
    }

    boolean isEmpty() {
        return n == 0;
    }
}