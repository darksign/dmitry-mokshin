package homework01.brackets;

public class StackTestMain {
    public static void main(String[] args) {
        Stack s = new Stack(10);
        s.push('(');
        s.push(')');
        s.push('{');
        System.out.println(s.pop());
        System.out.println(s.pop());
        System.out.println(s.pop());
    }
}

