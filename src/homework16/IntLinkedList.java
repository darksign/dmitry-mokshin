package homework16;

public class IntLinkedList implements IntList {
    private Node first;   // первый узел списка
    private int n; // число эл-ов

    @Override
    public void add(int elem) {
        Node newNode = new Node(); // новый узел с данным знач-ем
        newNode.value = elem;

        if(first != null) {
            Node current = first;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        } else {
            first = newNode;
        }
        n++;
    }

    @Override
    public int get(int index) {
        int i = 0;
        Node node = first;
        while (i != index) {
            i++;
            node = node.next;
        }
        return node.value;
    }

    @Override
    public void remove(int index) {
        if (index < 0 || index >= n) {
            throw new IndexOutOfBoundsException("No such element with index = " + index);
        }
        if (index == 0) {
            first = first.next;
        } else {
            int i = 0; // счетчик
            Node current = first;  // ссылка на текущий эл-т
            while (i < index - 1) {
                current = current.next;
                i++;
            }
            current.next = current.next.next;
        }
        n--;
    }

    @Override
    public int removeForPop() {
        Node oldFirst = first;
        first = first.next;
        return oldFirst.value;
    }

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public IntIterator iterator() {
        return new IntIteratorImpl();
    }

    class IntIteratorImpl implements IntIterator {
        Node current; // указатель на текущий эл-т

        public IntIteratorImpl() {
            current = first;
        }

        @Override
        public boolean hasNext() {
            return current.next != null;
        }

        @Override
        public int next() {
            int valueToReturn = current.value;
            current = current.next;
            return valueToReturn;
        }
    }

    class Node {    // служебный, не исп-ся в других
        int value;
        Node next;
    }

    public Node getFirst() {
        return first;
    }

    public int size() {
        return n;
    }
}