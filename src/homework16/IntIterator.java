package homework16;

public interface IntIterator {
    boolean hasNext();
    int next();
}
