package homework16;

public class IntListMain {
    public static void main(String[] args) {
        IntList list = new IntLinkedList();
        IntIterator iter = list.iterator();
        int sum = 0;
        while (iter.hasNext()) {
            sum += iter.next();
        }
        iter = list.iterator();
        System.out.println(sum);
    }
}