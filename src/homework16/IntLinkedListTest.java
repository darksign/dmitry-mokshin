package homework16;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IntLinkedListTest {
    IntLinkedList intLinkedList;

    @Before
    public void setUp() {
        this.intLinkedList = new IntLinkedList();
    }

    @Test
    public void addTest() {
        int num = 29;
        intLinkedList.add(num);
        int checked = intLinkedList.get(0);
        IntIterator iterator = intLinkedList.iterator();
        while (iterator.hasNext()) {
            checked = iterator.next();
        }
        assertEquals(num, checked);
    }

    @Test(expected = NullPointerException.class)
    public void addNull() {
        Integer nol = null;
        intLinkedList.add(nol);
    }

    @Test
    public void getTest() {
        int num = 1234;
        intLinkedList.add(num);
        intLinkedList.add(num);
        IntIterator iterator = intLinkedList.iterator();
        int checked;
        do {
            checked = iterator.next();
        } while (iterator.hasNext());
        assertEquals(intLinkedList.get(intLinkedList.size() - 1), checked);
    }

    @Test(expected = NullPointerException.class)
    public void removeTest() {
        int num = 666;
        intLinkedList.add(num);
        intLinkedList.remove(0);
        intLinkedList.get(0);
    }

    @Test(expected = NullPointerException.class)
    public void getNotExistNum() {
        IntIterator iterator = intLinkedList.iterator();
        Integer checked = null;
        while (iterator.hasNext()) {
            checked = iterator.next();
        }
        assertEquals(intLinkedList.get(intLinkedList.size()), (int)checked);
    }

    @Test
    public void isEmpty() {
        intLinkedList.add(666);
        intLinkedList.add(322);
        intLinkedList.remove(0);
        assertFalse(intLinkedList.isEmpty());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeNegNum() {
        int neg = -666;
        intLinkedList.remove(neg);
    }
}