package controlwork.task01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Predicate;

public class TextAnalyzer {
    public static void main(String[] args) throws FileNotFoundException {
        Map result = uniqueWords(o -> ((String) o).charAt(1) == 'a');
        List list = new ArrayList(result.entrySet());
        Collections.sort(list, (Comparator<Map.Entry<Integer, Integer>>) (a, b) -> b.getValue() - a.getValue());
        System.out.println(list.toString());
    }


    public static Map<String, Integer> uniqueWords(Predicate predicate) throws FileNotFoundException {
        Map<String, Integer> uniqueStrings = new TreeMap<>();
        File f = new File("words.txt");
        Scanner in = new Scanner(f);
        while (in.hasNext()) {
            String word = in.next();
            if (predicate.test(word))
                if (uniqueStrings.containsKey(word))
                    uniqueStrings.put(word, uniqueStrings.get(word) + 1);
                else uniqueStrings.put(word, 1);
        }
        Map<String, Integer> result = new TreeMap<>();
        uniqueStrings.entrySet().stream().sorted(Comparator.comparing(t -> t.getValue())).forEach(t -> result.put(t.getKey(), t.getValue()));
        return result;
    }
}
