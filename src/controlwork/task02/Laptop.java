package controlwork.task02;

public class Laptop {

    public Laptop(int screen_size, int RAM, int HDD) {
        this.screen_size = screen_size;
        this.RAM = RAM;
        this.HDD = HDD;
    }

    public int getScreen_size() {
        return screen_size;
    }

    public void setScreen_size(int screen_size) {
        this.screen_size = screen_size;
    }

    public int getRAM() {
        return RAM;
    }

    public void setRAM(int RAM) {
        this.RAM = RAM;
    }

    public int getHDD() {
        return HDD;
    }

    public void setHDD(int HDD) {
        this.HDD = HDD;
    }

    private int screen_size;
    private int RAM;
    private int HDD;

    @Override
    public String toString() {
        return "Laptop{" +
                "screen_size=" + screen_size +
                ", RAM=" + RAM +
                ", HDD=" + HDD +
                '}';
    }
}