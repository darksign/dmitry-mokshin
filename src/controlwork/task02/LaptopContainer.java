package controlwork.task02;

import java.util.ArrayList;
import java.util.Comparator;

public class LaptopContainer {
    private ArrayList<Laptop> laptops;

    public LaptopContainer() {
        laptops = new ArrayList<>();
    }

    public void add(Laptop laptop) {
        laptops.add(laptop);
    }

    public void remove(Laptop laptop) {
        laptops.remove(laptop);
    }

    public Laptop[] top10(Comparator comparator, int n) {
        laptops.sort(comparator);
        Laptop[] newList = new Laptop[n];
        for (int i = 0; i < n; i++) {
            newList[i] = laptops.get(i);
        }
        return newList;
    }


}
