package controlwork.task02;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Laptop l1 = new Laptop(1,2,3);
        Laptop l2 = new Laptop(7,2,1);
        Laptop l3 = new Laptop(2,6,2);
        Laptop l4 = new Laptop(5,1,1);
        LaptopContainer laptopContainer = new LaptopContainer();
        laptopContainer.add(l1);
        laptopContainer.add(l2);
        laptopContainer.add(l3);
        laptopContainer.add(l4);
        ScreenComparator screenComparator = new ScreenComparator();
        System.out.println(Arrays.toString(laptopContainer.top10(screenComparator, 3)));
    }
}