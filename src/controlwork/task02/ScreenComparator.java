package controlwork.task02;

import java.util.Comparator;

public class ScreenComparator implements Comparator<Laptop> {
    @Override
    public int compare(Laptop o1, Laptop o2) {
        return o2.getScreen_size() - o1.getScreen_size();
    }
}