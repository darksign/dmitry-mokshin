package controlwork.task03;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Input extends Thread {

    private CharContainer   charContainer;
    private FileInputStream fileInputStream;

    public Input(CharContainer charContainer, String filename) throws FileNotFoundException {
        this.charContainer = charContainer;
        fileInputStream = new FileInputStream(filename);
    }

    @Override
    public void run() {
        while (true) {
            synchronized (charContainer) {
                while (!charContainer.isConsumed()) {
                    try {
                        charContainer.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }
                int a = -1;
                try {
                    a = fileInputStream.read();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                charContainer.setCode(a);
                charContainer.produce();
                charContainer.notify();
                if (a == -1) return;

            }
        }
    }
}