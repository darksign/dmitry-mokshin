package controlwork.task03;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Output extends Thread {
    private CharContainer charContainer;
    private FileOutputStream fileOutputStream;

    public Output(CharContainer charContainer, String filename) throws FileNotFoundException {
        this.charContainer = charContainer;
        fileOutputStream = new FileOutputStream(filename);
    }

    @Override
    public void run() {
        while (true) {
            synchronized (charContainer) {
                while (!charContainer.isProduced()) {
                    try {
                        charContainer.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }
                if (charContainer.getCode() == -1) break;
                try {
                    fileOutputStream.write((char) charContainer.getCode());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                charContainer.consume();
                charContainer.notify();
            }
        }
    }
}
