package controlwork.task03;

public class CharContainer {
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    private int code;  // код символа
    private boolean isReady;

    public boolean isProduced() {
        return isReady;
    }

    public boolean isConsumed() {
        return !isReady;
    }

    public void produce() {
        isReady = true;
    }

    public void consume() {
        isReady = false;
    }
}