package controlwork.task03;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        String input  = "text1.txt";
        String output = "text2.txt";

        CharContainer p = new CharContainer();
        Input in = new Input(p, input);
        Output out = new Output(p, output);

        in.start();
        out.start();
    }
}