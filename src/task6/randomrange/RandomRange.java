package task6.randomrange;

public class RandomRange {

    public double randomRange(int min, int max) {
        return Math.random() * (max - min) + min;
    }
}
