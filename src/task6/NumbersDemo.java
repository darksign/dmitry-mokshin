package task6;

public class NumbersDemo {
    public static void main(String[] args) {
        double a = -191.675d;
        double b = 43.74d;
        int c = 16, d = 45;

        System.out.println("The absolute value of " + a + " is "+ Math.abs(a));
        System.out.println("The ceiling of " + b + " is "+ Math.ceil(b));
        System.out.println("The floor of " + b + " is "+ Math.floor(b));
        System.out.println("The rint of " + b + " is "+ Math.rint(b));
        System.out.println("The round of " + b + " is "+ Math.round(b));
        System.out.println("The max of " + c + " and " + d + " is "+ Math.max(c,d));
        System.out.println("The min of " + c + " and " + d + " is "+ Math.min(c,d));


    }
}
