package task6.figures;

public class Rectangle {
    private Point origin;
    private double diam = 0f;
    private double radians = 0f;

    public Rectangle() {
        origin = new Point(0, 0);
    }
    public Rectangle(Point p) {
        origin = p;
    }
    public Rectangle(double diam, double radians) {
        origin = new Point(0, 0);
        this.diam = diam;
        this.radians = radians;
    }
    public Rectangle(Point p, double diam, double radians) {
        origin = p;
        this.diam = diam;
        this.radians = radians;
    }
    public void move(int x, int y) {
        origin.x =  x;
        origin.y =  y;
    }
    public double getArea() {
        return Math.pow(diam, 2) * Math.sin(radians) / 2;
    }
}
