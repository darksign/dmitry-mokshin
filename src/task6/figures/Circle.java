package task6.figures;

public class Circle {
    private double diam; // диаметр
    private double circom; // площадь круга

    public Circle(double diam) {
        this.diam = diam;
    }

    public double getCircom() {
        return circom;
    }

    public double getDiam() {
        return diam;
    }

    public void сArea() {
        circom = 3.14 * Math.pow(this.diam, 2) / 4;
    }
}
