package audiotracks;

import java.util.Comparator;

public class Playlist {
    private final static double COEFFICIENT = 2;
    private static final int SIZE = 10;
    private AudioTrack[] tracklist;
    private int n;
    Comparator<AudioTrack> comparator;

    public Playlist() {
        tracklist = new AudioTrack[SIZE];
        this.n = 0;
    }

    public Playlist(Comparator<AudioTrack> comparator) {
        this();
        this.comparator = comparator;
    }

    public void add(AudioTrack track) {
        int i = 0;
        while (i < n) {
            if (comparator != null) {
                if (comparator.compare(track, tracklist[i]) < 0) {
                    break;
                }
            } else {
                if (track.compareTo(tracklist[i]) < 0) {
                    break;
                }
            }
            i++;
        }
        //if (n == tracklist.length) {
           /* AudioTrack[] newArray = new AudioTrack[][(int) (tracklist.length * COEFFICIENT)];
            for (int l = 0; l < tracklist.length; l++) {
                newArray[l] = tracklist[l];
            }
            tracklist = newArray;
        for (int c = n-1; c > i; c--) {
            tracklist[c + 1] = tracklist[c];
        } */ //fixme
    }
}