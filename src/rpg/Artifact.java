package rpg;

public class Artifact {
    private String type;
    private int hp;
    private int strength;
    private int armor;
    private String[] Types = {"weapon", "helmet", "chestplate", "boots", "shield"};


    public String getType() {
        return type;
    }

    public int getHp() {
        return hp;
    }

    public int getStrength() {
        return strength;
    }

    public int getArmor() {
        return armor;
    }

    public Artifact(String type, int hp, int strength, int armor) {
        boolean found = false;
        for (String type1 : Types){
            if(type.equals(type1)) {
                found = true;
                this.type = type;
                this.hp = hp;
                this.strength = strength;
                this.armor = armor;
            }
        }

        if (!found){
            throw new RuntimeException("Wrong artifact type " + type);
        }
    }

    public void getStats() {
        System.out.println("Type: " + type);
        System.out.println("hp: " + hp);
        System.out.println("strength: " + strength);
        System.out.println("armor: " + armor);
    }
}