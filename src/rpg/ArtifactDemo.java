package rpg;

public class ArtifactDemo {
    public static void main(String[] args) {
        Artifact art0 = new Artifact("weapon", 5, 10, 0);
        Artifact art1 = new Artifact("helmet", 10, 2, 7);
        Artifact art2 = new Artifact("chestplate", 10, 2, 7);
        Artifact art3 = new Artifact("boots", 5, 3, 4);
        Artifact art4 = new Artifact("shield", 12, 0, 12);

        Character char1 = new Character("First");
        Character char2 = new Character("Second");
        char1.info();
        char2.info();
        char1.addArtifact(art0);
        char2.addArtifact(art1);
        char1.info();
        char2.info();
    }
}
