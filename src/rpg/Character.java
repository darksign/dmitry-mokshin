package rpg;

import java.util.concurrent.ThreadLocalRandom;

public class Character {
    protected String name;
    protected int level = 1;
    protected int hp = 100;
    protected int originalHP = 100;
    protected int strength = 10;
    protected int originalStrength = 10;
    protected int armor = 0;
    protected int originalArmor = 0;
    protected Artifact[] arts = new Artifact[5];

    public Character() {}

    public Character(String name) {
        this.name = name;
    }

    public double getMinHit() {
        double i = strength * 0.5 - level;
        return i;
    }

    public double getMaxHit() {
        double i = strength * 1.0 + level;
        return i;
    }

    public int getHp() {
        return hp;
    }

    public int getArmor() {
        return armor;
    }

    public String getName() {
        return name;
    }

    public boolean isAlive() {
        return hp > 0;
    }

    public boolean isDead() {
        return hp <= 0;
    }

    public Artifact getArtifact(int i) {
        return arts[i];
    }

    public int hit() {
        int min = (int)getMinHit();
        int max = (int)getMaxHit();
        int randomNum = ThreadLocalRandom.current().nextInt(min, max +1);
        return randomNum;
    }

    public void getHit(int hit) {
        if (armor < hit )
        hp = hp - (hit - armor);
        else if (armor >= hit)
            hp = hp;
    }

    public void info() {
        System.out.println("Name: " + name);
        System.out.println("--------------");
        System.out.println("Level: " + level);
        System.out.println("HP: " + hp);
        System.out.println("Strength:" + strength);
        System.out.println("Armor: " + armor);
        System.out.println();
    }

    public void addArtifact(Artifact art) {
        switch(art.getType()) {
            case "weapon":
                arts[0] = art;
                break;
            case "helmet":
                arts[1] = art;
                break;
            case "chestplate":
                arts[2] = art;
                break;
            case "boots":
                arts[3] = art;
                break;
            case "shield":
                arts[4] = art;
                break;
        }
        recalcStats();
    }

    public void recalcStats() {
        hp = originalHP;
        strength = originalStrength;
        armor = originalArmor;
        for (Artifact artifact: arts){
            if (artifact != null) {
                hp = hp + artifact.getHp();
                strength = strength + artifact.getStrength();
                armor = armor + artifact.getArmor();
            }
        }
    }
}
