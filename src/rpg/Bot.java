package rpg;

import  java.util.Scanner;

public class Bot extends Character {

    public Bot(String name){
        super.name = name;
    }

    int num = (int) (Math.random() * 4 + 1);
    Scanner in = new Scanner(System.in);
    public void botKilled(Character player){
        for (int i = 0; i < arts.length; i++){   // цикл по артефактам бота
            if (arts[i] != null)                 // проверка наличия артефакта
                if ((num != 2) && (num != 1)) {  // вер-ть выпадения = 50%
                    System.out.println("Artifact has dropped!");
                    arts[i].getStats();
                    if (player.getArtifact(i) != null) {   // проверка наличия арт-та у игрока
                        System.out.println();
                        System.out.println("Your artifact's stats: ");
                        player.getArtifact(i).getStats();
                    }
                    else System.out.println("You haven't a such artifact!");
                    System.out.println();
                    System.out.println("Would you like to equip the artifact?" + "\n" + "y/n?");
                    String ans = in.nextLine();
                    if (ans.equals("y")) {
                        System.out.println("Your new artifact's stats:");
                        player.addArtifact(arts[i]);
                        player.getArtifact(i).getStats();
                    }
                    else if (ans.equals("n"))
                        System.out.println("You haven't equip the artifact!");
                }
        }
    }
}
