package rpg;

public class BattleDemo {
    public static void main(String[] args) {
        Character char1 = new Character("John");
        Character char2 = new Character("Michael");

        do {
            System.out.println("-----------");
            int hit = char1.hit();
            int firstHP = char2.getHp();
            char2.getHit(hit);
            System.out.println(char1.getName() +"(" + char1.getHp() + ")" + " hits " + char2.getName() + "(" + firstHP + ")-" + hit);
            if (char2.isDead()){
                System.out.println(char2.getName() + " is dead");
                break;
            }
            int hit2 = char2.hit();
            int secondHP = char1.getHp();
            char1.getHit(hit2);
            System.out.println(char2.getName() +"(" + char2.getHp() + ")" + " hits " + char1.getName() + "(" + secondHP + ")-" + hit2);
            if (char1.isDead()){
                System.out.println(char1.getName() + " is dead");
                break;
            }
        } while (char1.isAlive() && char2.isAlive());
    }
}

