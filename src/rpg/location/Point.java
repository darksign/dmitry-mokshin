package rpg.location;

public class Point {
    private char simbol = '*';

    public int getX() {
        return x;
    }

    private int x;

    public int getY() {
        return y;
    }

    private int y;

    public char simb() {
        return simbol;
    }

    public void changeCoord(char input) {
        if (input == 'a')
            y = y-1;
        if (input == 's')
            x = x+1;
        if (input == 'w')
            x = x-1;
        if (input == 'd')
            y = y+1;
    }

    public void setCoord(int x, int y) {
        this.x = x;
        this.y = y;
    }
}