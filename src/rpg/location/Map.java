package rpg.location;

public class Map {
    private char[][] field = new char[6][6];  // квадратная!!!
    private Point point;

    public int getFieldLength() {
        return field[0].length;
    }

    public Map(Point point) {
        for (int i = 0; i < field[0].length; i++) {
            for (int j = 0; j < field[0].length; j++)
                field[i][j] = ' ';
        }
        this.point = point;
    }

    public void place(int x, int y) {
        point.setCoord(x,y);
        field[x][y] = point.simb();
    }

    public char getFill(int x, int y) {
        return field[x][y];
    }

    public void movePoint(char input) {
        field[point.getX()][point.getY()] = ' ';

        point.changeCoord(input);
        field[point.getX()][point.getY()] = point.simb();
    }
}