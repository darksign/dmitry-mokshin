package rpg.location;

import java.util.Scanner;

public class LocationDemo {
    public static void main(String[] args) {
        Point point = new Point();
        boolean run = true;
        Scanner in = new Scanner(System.in);
        Map map = new Map(point);
        MapDrawer drawer = new MapDrawer();
        drawer.mapDraw(map);
        System.out.println("Введите координаты точки, в которую хотите поставить символ:");
        String a = in.next();
        char b = a.charAt(0);
        int x = b - 97;
        int y = in.nextInt();
        map.place(x, y);
        drawer.mapDraw(map);
        while (run){
            System.out.println("Введите a/s/w/d для перемещения символа или введите 'x' для выхода из программы:");
            a = in.next();
            b = a.charAt(0);
            if (b=='x') {
                run = false;
                break;
            }
            map.movePoint(b);
            drawer.mapDraw(map);

        }
    }
}