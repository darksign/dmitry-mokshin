package rpg;

public class BotDemo {
    public static void main(String[] args) {
        Artifact art0 = new Artifact("weapon", 5, 10, 0);
        Artifact art1 = new Artifact("helmet", 10, 2, 7);
        Artifact art2 = new Artifact("chestplate", 10, 2, 7);
        Artifact art3 = new Artifact("boots", 5, 3, 4);
        Artifact art4 = new Artifact("shield", 12, 0, 12);
        Artifact art5 = new Artifact("weapon", 0, 1, 0);

        Bot bot = new Bot("Bot");
        Character char1 = new Character("Kevin");
        bot.addArtifact(art5);
        char1.addArtifact(art0);

        do {
            System.out.println("-----------");
            int hit = char1.hit();
            int firstHP = bot.getHp();
            bot.getHit(hit);
            System.out.println(char1.getName() +"(" + char1.getHp() + ")" + " hits " + bot.getName() + "(" + firstHP + ")-" + hit);
            if (bot.isDead()){
                System.out.println(bot.getName() + " is dead");
                System.out.println();
                bot.botKilled(char1);
                break;
            }
            int hit2 = bot.hit();
            int secondHP = char1.getHp();
            char1.getHit(hit2);
            System.out.println(bot.getName() +"(" + bot.getHp() + ")" + " hits " + char1.getName() + "(" + secondHP + ")-" + hit2);
            if (char1.isDead()){
                System.out.println(char1.getName() + " is dead");
                break;
            }
        } while (char1.isAlive() && bot.isAlive());


    }
}
