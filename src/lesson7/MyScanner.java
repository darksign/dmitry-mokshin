package lesson7;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class MyScanner {
    private InputStream is;

    public MyScanner(InputStream is) {
        this.is = is;
    }

    public int nextInt() {
        return 0;
    }

    public double nextDouble() {
        return 0;
    }

    public String next() throws IOException {
        byte[] bytes = new byte[16];
        int i;
        int j = 0;
        while ((i = is.read()) != 32){
            bytes[j] = (byte)i;
            j++;
        }
        int length = j-1;
        String str = new String(bytes, 0, length, StandardCharsets.UTF_8);
        return str;
    }

    public String nextLine() {
        return null;
    }
}