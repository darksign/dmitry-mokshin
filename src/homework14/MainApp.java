package homework14;

import java.util.Random;
import java.util.Scanner;

public class MainApp {
    public static int sum = 0;
    public static int sumMain = 0;

    public static void main(String[] args) throws InterruptedException {
        //заведите массив на ~1 000 000, заполните его рандомом
        //заведите несколько counterThread'ов (n штук),
        //посчитайте сумму с их помощью (в n потоках),
        //результат будет в переменной sum в MainApp
        //а затем посчитайте for'ом прямо в main'е,
        //сравните результаты

        Random r = new Random();
        int[] nums = new int[1000000];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = r.nextInt(100);
        }

        Scanner in = new Scanner(System.in);
        System.out.println("Введите четное число потоков (2-1000000):");
        int n = in.nextInt();
        Thread[] counterThreads = new CounterThread[n];
        int amountOfNumbersForEachThread = nums.length / n;  // кол-во элементов массива для каждого потока


        for (int i = 0; i < n; i++) {  // инициализируем потоки
            counterThreads[i] = new CounterThread(i*amountOfNumbersForEachThread, ((i+1)*amountOfNumbersForEachThread)-1, nums);
            counterThreads[i].start();
        }

        for (int i = 0; i < n; i++) {
            counterThreads[i].join();
        }

        for (int i = 0; i < nums.length; i++) {
            sumMain += nums[i];
        }

        System.out.println("Сумма по потокам: " + sum);
        System.out.println("Сумма в main: " + sumMain);
    }
}
