package map;

/**
 * Create a field with a symbol
 */
public class MapArea {
    /**
     * field is square!
     */
    private char[][] field = new char[6][6];
    private Point point;

    /**
     * A method sets a field element where the point is
     * @param a
     */
    public void resetCell(char a) {
        field[point.getX()][point.getY()] = a;
    }

    /**
     * Sets an empty field
     * @param point
     */
    public MapArea(Point point) {
        for (int i = 0; i < field.length; i++)
            for (int j = 0; j < field[0].length; j++)
                field[i][j] = ' ';
        this.point = point;
    }

    /**
     * Sets symbol and point coordinates
     * @param simbol
     * @param x
     * @param y
     * @see Point
     */
    public void placeObject(char simbol, int x, int y){
        point.setSimbol(simbol);
        point.setCoord(x,y);
        field[x][y] = simbol;
    }

    /**
     * Returns array length (array is square!)
     * @return length of array
     */
    public int getFieldLength() {
        return field.length;
    }

    /**
     * Returns a cell of the field
     * @param x
     * @param y
     * @return a cell of the field
     */
    public char getCell(int x, int y) {
        return field[x][y];
    }
}