package map;
/**
 * The class moves a point changing its coordinates
 */

public class MovePoint {
    /**
     *
     * @param point
     * @param mp
     */
    private Point point;
    private MapArea mp;

    public MovePoint(Point point, MapArea mp) {
        this.point = point;
        this.mp = mp;
    }

    /**
     * Change coordinates of the point
     * @param input
     */
    public void changeCoord(char input) {
        switch (input){
            case 'a':
                point.setX(point.getX() + Direction.LEFT.getX());
                point.setY(point.getY() + Direction.LEFT.getY());
            break;
            case 's':
                point.setX(point.getX() + Direction.DOWN.getX());
                point.setY(point.getY() + Direction.DOWN.getY());
            break;
            case 'w':
                point.setX(point.getX() + Direction.UP.getX());
                point.setY(point.getY() + Direction.UP.getY());
            break;
            case 'd':
                point.setX(point.getX() + Direction.RIGHT.getX());
                point.setY(point.getY() + Direction.RIGHT.getY());
            break;
            default:
                System.out.println("Use a/s/w/d buttons!");
        }
    }

    /**
     * Resets value of a cell where the point was, changes the point position and its coordinates
     * and set symbol in the new point coordinates
     * @param input
     */
    public void movePoint(char input) {
        mp.resetCell(' ');
        changeCoord(input);
        mp.resetCell(point.getSimbol());
    }
}