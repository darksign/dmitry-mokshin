package map;

import java.util.Scanner;

public class MapDemo {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Point point = new Point();
        MapArea mp = new MapArea(point);
        MapDisplay drawer = new MapDisplay();
        MapCoords mc = new MapCoords();
        MovePoint movep = new MovePoint(point, mp);

        drawer.display(mp);
        System.out.println("Enter a symbol you want:");
        char simb = in.next().charAt(0);
        System.out.println("Enter point coordinates where you want to insert the symbol:"); //Enter point coordinates you want to insert the symbol
        String input = in.next();
        char x = input.charAt(0);
        int y = Integer.parseInt("" + input.charAt(1));
        mp.placeObject(simb, mc.getCoords(x,y)[0], mc.getCoords(x,y)[1]);
        drawer.display(mp);

        while (true) {
            System.out.println("Enter a/s/w/d to move the symbol, or \"x/X\" to exit the program:");
            String a = in.next();
            char b = a.charAt(0);
            if (b == 'x' || b == 'X') {
                break;
            }
            movep.movePoint(b);
            drawer.display(mp);
        }
        System.out.println("End of work...");
    }
}