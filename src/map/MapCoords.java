package map;

/**
 * Converts input coordinates into array(field) coordinates
 */
public class MapCoords {
    private int[] coords = new int[2];

    /**
     * Returns converted coordinates
     * @param a
     * @param y
     * @return array of coordinates (2 numbers)
     */
    public int[] getCoords(char a, int y){
        coords[0] = a - 97;
        coords[1] = y;
        return coords;
    }
}