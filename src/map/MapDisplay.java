package map;

/**
 * The class designed to draw the map
 */
public class MapDisplay {
    /**
     * @see MapArea
     * @param a
     */
    public void display(MapArea a) {
        System.out.print("   ");
        for (int y = 0; y < a.getFieldLength(); y++)
            System.out.print(y + " ");
        System.out.println();
        for (int i = 97; i < 97 + a.getFieldLength(); i++) {
            char k = (char) i;
            System.out.print(k + " |");
            for (int j = 0; j < a.getFieldLength(); j++)
                System.out.print(a.getCell((int)i - 97, j) + "|");
            System.out.println();
        }
    }
}