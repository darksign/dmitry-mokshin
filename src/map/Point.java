package map;

/**
 Keeps data of a symbol (coordinates and description)
 */
public class Point {
    private char simbol;

    private int x;
    private int y;

    /**
     * sets y coordinate
     * @param y
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * sets x coordinate
     * @param x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Sets symbol which a user has chosen
     * @param simbol
     */
    public void setSimbol(char simbol) {
        this.simbol = simbol;
    }

    /**
     * Sets coordinates
     * @param x
     * @param y
     */
    public void setCoord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * gets x coordinate
     * @return x
     */
    public int getX() {
        return x;
    }

    /**
     * gets x coordinate
     * @return y
     */
    public int getY() {
        return y;
    }

    /**
     * Gets symbol that the class keeps
     * @return symbol
     */
    public char getSimbol() {
        return simbol;
    }
}