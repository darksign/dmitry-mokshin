package map;

/**
 * This enum-class holds value of movements on the map
 */
public enum Direction {
    UP(-1, 0),
    RIGHT(0, 1),
    DOWN(1, 0),
    LEFT(0, -1);

    private final int x;
    private final int y;

    private Direction(int x, int y){
        this.x = x;
        this.y = y;
    }

    /**
     * Returns x coordinate of a direction
     * @return x coordinate
     */
    public int getX(){
        return x;
    }

    /**
     * Returns y coordinate of a direction
     * @return y coordinate
     */
    public int getY(){
        return y;
    }
}
