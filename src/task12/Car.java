package task12;

public class Car implements Fuelable, Driveable{  //заправляемый, водимый
    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public int getFuel() {
        return fuel;
    }

    private int fuel = 0;

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    private int distance = 0;
}
