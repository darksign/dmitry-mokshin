package task12;

public class Task12Demo {
    public static void main(String[] args) {
        Car car = new Car();
        GasStation gs = new GasStation();

        gs.setFuel(car);
        System.out.println(car.getFuel());

        Driver driver = new Driver();
        driver.drive(car, 100);
        System.out.println(car.getDistance());
    }
}
