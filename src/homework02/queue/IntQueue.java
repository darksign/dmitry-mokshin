package homework02.queue;

public interface IntQueue {
    void enqueue(int elem);
    int dequeue();
}
