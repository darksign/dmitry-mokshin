package homework02.queue;

public class QueueInt implements IntQueue {
    private final static int INITIAL_CAPACITY = 10;
    private final static double COEFFICIENT = 1.5;
    private int[] arr;
    public int n;  // ??? почему нужен public ???
    private  int rear;
    private int first;
    private int start;


    @Override
    public void enqueue(int elem) {
        if (rear == arr.length - 1) {
            rear = -1;
        }
        arr[++rear] = elem;
        n++;
    }

    @Override
    public int dequeue() {
        start = arr[first++];
        if (first == arr.length){
            first = 0;
    }
        n--;
        return start;
    }

    public QueueInt() {
        arr = new int[INITIAL_CAPACITY];
        n = 0;
        rear = -1;
        first = 0;
    }
}
