package homework02.intlinkedlist;

public interface IntIterator {
    boolean hasNext();
    int next();
}
