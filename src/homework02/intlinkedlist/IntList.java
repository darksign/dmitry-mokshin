package homework02.intlinkedlist;

public interface IntList {
    void add(int n);
    int get(int index);
    void remove(int index);
    boolean isEmpty();
    IntIterator iterator();
    int removeForPop();
}
