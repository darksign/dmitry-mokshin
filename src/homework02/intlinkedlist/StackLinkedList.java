package homework02.intlinkedlist;

public class StackLinkedList {
    private IntLinkedList linked = new IntLinkedList();

    void push(int c){
        if ( linked.getFirst().next == null){
            throw new IllegalStateException("Stack is already full");
        }
        linked.add(c);
    }

    int pop() {
        return linked.removeForPop();
    }

    public boolean isEmpty(){
        return (linked.getFirst().value == 0);
    }
}
