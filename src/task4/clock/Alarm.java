package task4.clock;

public class Alarm {
    public Clock c;

    public Alarm (Clock c) {
        this.c = c;
    }

    public void Dzzz(){
        int k = c.timer;
        for (int i = 0; i < k; i++) {
            c.timer -= 1;
            if ((c.timer % 50) == 0) {
                System.out.println("Звонок!");
                System.out.println(c.timer);
            }
        }
    }
}
