package task4.tv;

public class Pult {
    public Screen screen;

    public Pult(Screen screen){
        this.screen = screen;
    }

    public void switcher(int l){
        if (l == 0)
            screen.is_On = false;
        if (l == 1)
            screen.is_On = true;
    }
}
