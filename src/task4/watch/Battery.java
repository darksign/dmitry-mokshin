package task4.watch;

public class Battery {
    public int capacity = 10;

    public void decrease () {
        capacity -= 1;
    }

    public void charge (int x){
        capacity += x;
    }
}
