package task4.watch;

public class Watch {
    public Battery battery;

    public Watch (Battery b) {
        battery = b;
    }

    public void tick (){
        battery.decrease();
    }
}
