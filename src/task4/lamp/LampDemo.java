package task4.lamp;

public class LampDemo {
    public static void main(String[] args) {
        Lamp lamp = new Lamp();
        Switcher s = new Switcher(lamp);

        s.switch_Off();
        System.out.println();
        s.switch_On();
    }
}
