package task4.lamp;

public class Switcher {
    public Lamp lamp;

    public Switcher(Lamp lamp){
        this.lamp = lamp;
    }

    public void switch_On() {
         this.lamp.is_On = true;
         System.out.println(lamp.is_On);
         System.out.println("Лампа включена");
    }

    public void switch_Off() {
        this.lamp.is_On = false;
        System.out.println(lamp.is_On);
        System.out.println("Лампа выключена");
    }
}
