package stream;

import java.util.*;
import java.util.stream.*;

public class MyArrayList<E> {
    private E[] elements;
    private int count = 0;

    public MyArrayList() {
        elements = (E[]) new Object[10];
    }

    public void add(E element) {
        if (count == elements.length) {
            E[] tmpArray = Arrays.copyOf(elements, (int) (elements.length * 2));
            elements = tmpArray;
        }
        elements[count++] = element;
    }

    private List<E> toList() {
        List<E> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            list.add(elements[i]);
        }
        return list;
    }

    public Stream stream() {
        return new MyStream();
    }
}