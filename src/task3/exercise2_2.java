package task3;
import java.util.Scanner;

public class exercise2_2 {

    public static void main(String[] args) {
        String s = "";
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите кол-во строк: ");
        int n = scan.nextInt();

        for (int i  = 0; i < n; i++) {
            for (int j = 0; j < n-i; j++)
                System.out.print("*");
            for (int a = 0; a < i; a++)
                System.out.print(" ");
            System.out.println();
        }
    }
}
