package task3;

import java.util.Scanner;

public class exercise3 {

    public static void main(String[] args) {
        String s = "*";
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите кол-во строк: ");
        int n = scan.nextInt();
        int m = n*2-1;
        for (int i  = 1; i <= n; i++) {
            for (int a = 0; a < (m-(i*2-1))/2; a++)
                System.out.print(" ");
            System.out.print(s);
            for (int j = 0; j < (m-(i*2-1))/2; j++)
                System.out.print(" ");
            s = s + "**";
            System.out.println();
        }
    }
}
