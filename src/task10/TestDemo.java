package task10;

import java.util.Scanner;

public class TestDemo {
    public static void main(String[] args) {
        String[] questions = {"How many colors are in the rainbow?", "What colors are in the Russian flag?", "Who is the president of Russian Federation now?", "Which answer is right? :)", "How many fingers does human have?"};
        String[] answers = {"Six", "Five", "*Seven", "Red, blue and green", "*Red, blue and white", "Red, green and white", "Dmitry Medvedev", "*Vladimir Putin", "Michael Gorbachev", "*Right", "Wrong", "I don't know", "3", "*20", "5"};

        Scanner in = new Scanner(System.in);
        int n = 0;
        int right_answers = 0;  // кол-во правильных ответов

        for (int i = 0; i < questions.length; i++) {     // цикл вопросов
            int right = 0;                           // правильный ответ на текущий вопрос

            for (int j = 0; j < 3; j++) {        // цикл ответов на текущий вопрос
                char c = answers[n + j].charAt(0); // проверяем наличие *
                String a = "" + c;   //с == '*';
                if (a.equals("*")) {                              // если эл-т имеет *, то выходим из цикла, переприсваивая эл-т без *
                    answers[n + j] = answers[n + j].substring(1);
                    right = j+1;
                    break;
                }
            }

            System.out.println(questions[i]); // вывод вопроса

            System.out.println("1. " + answers[n] + "\n" + "2. " + answers[n + 1] + "\n" + "3. " + answers[n + 2]);  // вывод вариантов ответа
            int input = in.nextInt();

            if (input == right) {
                System.out.println("You're right!");
                right_answers++;
            } else System.out.println("You're wrong!");

            n += 3;
        }

        System.out.println("Your result is " + right_answers + "/5");
    }
}