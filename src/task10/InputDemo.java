package task10;

import java.util.Scanner;

public class InputDemo {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean run = true;
        System.out.println("Hi! What's your name?");
        System.out.print(">");
        String name = in.nextLine();
        System.out.println("Hi, " + name + "!");
        System.out.println("How old are you?");
        System.out.print(">");
        String i = in.nextLine();
        int age = Integer.parseInt(i);
        System.out.println("Ask any questions, my friend.");
        System.out.print(">");
        do {
            String statement = in.nextLine();
            switch (statement) {
                case "Who are you?":
                    System.out.println("I'm a chatBot. I was made to help lonely people.");
                    System.out.print(">");
                    break;
                case "What do you know about me?":
                    System.out.println("I know your name and your age" + "\n" + "Name: " + name + "\n" + "---------" + "\n" + "Age: " + age);
                    System.out.print(">");
                    break;
                case "How are you?":
                    System.out.println("I'm OK, thank you!");
                    System.out.print(">");
                    break;
                case "Let's play football":
                    System.out.println("I can only play chess.");
                    System.out.print(">");
                    break;
                case "Let's play chess":
                    System.out.println("We haven't a board!");
                    System.out.print(">");
                    break;
                case "Bye":
                    run = false;
                    System.out.println("Name: " + name + "\n" + "----------" + "\n" + "Age: " + age);
                    System.out.println("Bye!");
                    break;
                default:
                    System.out.println("What do you mean?");
            }
        } while (run);
    }
}